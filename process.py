import pickle
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import execute as exe
import time

def polarization(file = 0, v = 0, make_plot = True, save_plot = False): 
    if file != 0:
        infile = open('%s_v'%(file),'rb')
        v = pickle.load(infile)
        infile.close()
    
    s = np.sqrt(np.sum(v[0,:,0]**2))
    [N,dim,T]=np.shape(v)
    v_t = np.sum(v,axis = 0)
    p = 1/N*np.sqrt(v_t[0,:]**2+v_t[1,:]**2+v_t[2,:]**2)/s
    if make_plot == True:
        plt.figure()
        plt.plot(p)
        plt.xlabel('timesteps')
        plt.ylabel('p_group')
        
    if save_plot == True:
        plt.savefig('polarization.png')
        
    return p

def group_centroid(file = 0,c = 0):
    if file != 0:
        infile = open('%s_c'%(file),'rb')
        c = pickle.load(infile)
        infile.close()
        
    [N,dim,T]=np.shape(c)
    c_group = 1/N*np.sum(c, axis = 0)
    return c_group


def angular_momentum(file = 0, c = 0, v = 0, make_plot = True,save_plot = False,m_vector = False):
    if file != 0:
        infile = open('%s_c'%(file),'rb')
        c = pickle.load(infile)
        infile.close()
        infile = open('%s_v'%(file),'rb')
        v = pickle.load(infile)
        infile.close()
        
    [N,dim,T] = np.shape(c)
    c_group = group_centroid(file = 0, c = c)
    r_ic = c-c_group
    m_t = np.sum(np.cross(r_ic,v,axisa=1,axisb=1),axis=0)
    m_group = 1/N*np.sqrt(m_t[:,0]**2+m_t[:,1]**2+m_t[:,2]**2)
    
    if make_plot == True:
        plt.figure()
        plt.plot(m_group)
        
    if save_plot == True:
        plt.savefig('angular_momentum.pdf')
    if m_vector == True:
        m_dir = m_t.T/(np.sqrt(np.sum(m_t**2,1)))
        m_dir = m_dir.T
        #these angles give the direction of the angular momentum vector
        theta = np.arccos(m_dir[:,2])*180/np.pi
        phi = np.arctan(m_dir[:,1]/m_dir[:,0])*180/np.pi
        return m_group, theta,phi
    
    return m_group

def plot_arrow(file,timesteps = 'last',width = 0, el = None, az = None, save_plot = False):
    """This function plots a 3D figure with the birds on (x,y,z) positions and the velocity as an arrow from that position, pointing in the direction of the velocity vector"""
    infile = open('%s_c'%(file), 'rb')
    c = pickle.load(infile)
    infile.close()
    infile = open('%s_v'%(file), 'rb')
    v = pickle.load(infile)
    infile.close()
    [N,dim,T] = np.shape(c)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #ax.zaxis.set_rotate_label(False) 
    #ax.set_zlabel('Z')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels([])

    #choose last time step
    if timesteps == 'last':
        c_last = c[:,:,T-1]
        X,Y,Z = c_last[:,0],c_last[:,1],c_last[:,2]
        v_last = v[:,:,T-1]
        U,V,W = v_last[:,0],v_last[:,1],v_last[:,2]
        ax.quiver(X,Y,Z,U,V,W)
        ax.view_init(elev = el,azim=az)
    #Show all timesteps in one plot
    elif timesteps == 'all':
        ax.quiver(c[:,0],c[:,1],c[:,2],v[:,0],v[:,1],v[:,2],length = 0.5)
        ax.view_init(elev = el,azim=az)
    #Show a certain timestep
    elif type(timesteps) == int and width == 0:
        t = timesteps
        c_t = c[:,:,t-1]
        X,Y,Z = c_t[:,0],c_t[:,1],c_t[:,2]
        v_t = v[:,:,t-1]
        U,V,W = v_t[:,0],v_t[:,1],v_t[:,2]
        ax.quiver(X,Y,Z,U,V,W)
        ax.view_init(elev = el,azim=az)
    #Show a bunch of timesteps
    elif type(timesteps) == int and width != 0:
        t = timesteps
        w = width
        c_t = c[:,:,t-1-w:t]
        X,Y,Z = c_t[:,0,:],c_t[:,1,:],c_t[:,2,:]
        v_t = v[:,:,t-1-w:t]
        U,V,W = v_t[:,0,:],v_t[:,1,:],v_t[:,2,:]
        ax.quiver(X,Y,Z,U,V,W)
        ax.view_init(elev = el,azim=az)
    #Wrong input 
    else:
        print('This is not the right input for timesteps. Use "last" of "all"')
    
    if save_plot == True:
        plt.savefig('plot_arrow.pdf')
        
def plot_area(name,name2,grid_size,dr_o_max,dr_a_max,eq_time,N_replicates = 10):
    infile = open('parameters','rb')
    [N,r_r,dr_o,dr_a,alpha,s,sigma,theta,tau,T] = pickle.load(infile)
    infile.close()
    
    N = N.astype(np.int)
    T = T.astype(np.int)
    dr_o = np.arange(0,dr_o_max,grid_size)
    dr_a = np.arange(0,dr_a_max,grid_size)
    N_o = np.size(dr_o)
    N_a = np.size(dr_a)
    p_mean = np.zeros([N_o,N_a])
    m_mean = np.zeros([N_o,N_a])
    p_var = np.zeros([N_o,N_a])
    m_var = np.zeros([N_o,N_a])
    X = np.zeros([N_o,N_a])
    Y = np.zeros([N_o,N_a])
    
    start_time = time.time()
    for i in range(N_o):
        print(i)
        print("1--- %s seconds ---" %(time.time() - start_time))
        for j in range(N_a):
            print(j)
            print("2--- %s seconds ---" %(time.time() - start_time))
            X[i,j] = dr_o[i]
            Y[i,j] = dr_a[j]
            
            p_store = np.zeros(N_replicates)
            m_store = np.zeros(N_replicates)
            k = 0
            q = 0
            
            for k in range(N_replicates):
                c,v = exe.execute(N,r_r,dr_o[i],dr_a[j],s,alpha,sigma,theta,tau,T,name,file = False)
                D = np.amax(np.sqrt(np.sum((c[:,:,T-1] - c[0,:,T-1])**2,1)))
                if D < N*(r_r + dr_o[i] + dr_a[j])/np.sqrt(3):
                    p = polarization(v = v, make_plot = False)
                    p_store[q] = np.mean(p[eq_time:T-1])
                    m_group = angular_momentum(c = c, v = v, make_plot = False)
                    m_store[q] = np.mean(m_group[eq_time:T-1])
                    q = q+1
                
            p_mean[i,j] = np.mean(p_store[0:q+1])
            m_mean[i,j] = np.mean(m_store[0:q+1])
            p_var[i,j] = np.var(p_store[0:q+1])
            m_var[i,j] = np.var(m_store[0:q+1])

    
    m_mean=m_mean/np.max(m_mean)
    fig_p = plt.figure()
    ax_p = fig_p.add_subplot(111, projection='3d')
    ax_p.plot_wireframe(X,Y,p_mean)
    plt.gca().invert_xaxis()
    ax_p.set_xlabel('dx_o')
    ax_p.set_ylabel('dx_a')
    ax_p.zaxis.set_rotate_label(False) 
    ax_p.set_zlabel('polarization')
    
    fig_m = plt.figure()
    ax_m = fig_m.add_subplot(111, projection='3d')
    ax_m.plot_wireframe(X,Y,m_mean)
    plt.gca().invert_xaxis()
    ax_m.set_xlabel('dx_o')
    ax_m.set_ylabel('dx_a')
    ax_m.zaxis.set_rotate_label(False) 
    ax_m.set_zlabel('angular momentum')
    
    mean_plot = np.array([X,Y,p_mean,m_mean,p_var,m_var])
    outfile = open(name2, 'wb')
    pickle.dump(mean_plot,outfile)
    outfile.close()
            
