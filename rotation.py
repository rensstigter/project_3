import numpy as np

def rotation(d_dir,v_dir,theta):
    """This function determines the rotation from the velocity vector v_dir towards the unit direction vector d_dir over an angle theta, implementing the Euler-Rodrigues formula. The outcome d_dir_new is the new direction the bird will fly in. This is not necesserily equal to d_dir because the bird can rotate a certain amount of radians in one timestep. 
    
    --- Input parameters
    d_dir = the desired direction the birds want to follow
    v_dir = the actual direction the bird follows
    theta = the angle the bird will turn in one timestep
    """

    N = np.int(np.size(d_dir)/3)
    #u is the vector around which v_dir can rotate towards d_dir, so u is perpendicular to both d_dir and v_dir:
    u = np.cross(v_dir,d_dir)
    #be sure that theta has the right shape:
    theta = theta.reshape(N,1)
    
    #check for zero vectors in u, for these we don't need to calculate the rotation matrix. For the other we calculate the rotation matrix
    if N != 1:
        index = np.any(u>0,1)
    else:
        index = np.nonzero(u>0)
        
    R = matrix(u[index],theta[index])
    
    d_dir_new = d_dir
    if np.any(R != 0):
        #calculate for the nonzero vectors in u the the new direction the birds will fly in:
        d_dir_new[index] = np.einsum('...jk,...k',R,v_dir[index])
    
    return d_dir_new

def matrix(u,theta):
    """This function calculates the rotation matrix R for a rotation around unit axis u with an angle theta. The rotation matrix is calculated using the Euler-Rodrigues formula
    
    --- Input parameters:
    u = unit vector around which the rotation takes place
    theta = the angle of the rotation
    """
    #check if the vector is sufficient (a 3D vector in space)
    if np.size(u) < 3:
        #print('matrix: size of u is smaller than 3')
        R = 0
        return R 
    else:
        N = np.int(np.size(theta))
        #different behaviour if u contains just 1 vector (shape = (3), or if u contains N vectors (shape = (N,3)))
        if N == 1:
            u_mag = np.sqrt(np.sum(u**2))
            if u_mag == 0:
                print('matrix: the magnitude of u is zero')
                R = 0
                return R 
            else:
                u = u/u_mag
        elif N > 1:
            u_mag = np.sqrt(np.sum(u**2,1))
            if np.any(u_mag == 0) == True:
                print('matrix: the magnitude of u is zero')
                R = 0
                return R
            else:
                u = (u.T/u_mag).T
        
        #calculate the Euler parameters:
        a = np.cos(theta/2)
        b,c,d = (u*np.sin(theta/2)).T
        b = b.reshape(N,1)
        c = c.reshape(N,1)
        d = d.reshape(N,1)
        
        #calculate each entry of the rotation matrix, and put them together to get an array with size(N,3,3), where the (3,3) part is the 3D rotation vector for a certain combination of velocity/direction unit vectors
        R11 = a**2+b**2-c**2-d**2
        R12 = 2*(b*c - a*d)
        R13 = 2*(b*d + a*c)
        R1 = np.dstack((R11,R12,R13))

        R21 = 2*(b*c+a*d)
        R22 = a**2+c**2-b**2-d**2
        R23 = 2*(c*d-a*b)
        R2 = np.dstack((R21,R22,R23))

        R31 = 2*(b*d-a*c)
        R32 = 2*(c*d+a*b)
        R33 = a**2+d**2-b**2-c**2
        R3 = np.dstack((R31,R32,R33))

        R = np.hstack((R1,R2,R3))   
        
    if R.shape != (N,3,3):
        if R.shape != (3,3):
            print('matrix: wrong shape for R, it should be', (N,3,3), R.shape)
            R = 0
        
    return R