import numpy as np

def init(N,r_r,dr_o,dr_a,s):
    """This function generates the initial positions and initial constant  velocity."""
    
    #from the paper: each individual start with random orientations and random positions within a sphere, where it can detect at least one other bird
    r_zone = r_r+dr_o+dr_a
    c = N**(1/3)*r_zone*np.random.rand(N,3)/np.sqrt(3)
    v = np.random.rand(N,3)-1/2
    v_len = np.sqrt(v[:,0]**2+v[:,1]**2+v[:,2]**2)
    v = v.T/v_len*s
    v = v.T
    return c, v