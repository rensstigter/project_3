import numpy as np
import del_diag as dd
import magic_matrix as mm
import rotation as rt

def r_ij(N,r_r,dr_o,dr_a,c):
    """This function return to matrices, dc and dr. dc is a (N-1,N,3) matrix in which every column corresponds to 1 bird and gives the relative distances to all other birds, in x,y,z. dr is a (N-1,N) matrix in which every column corresponds to a bird and gives the relative radii to all other birds. """
    
    dc = np.zeros([N-1,N,3])
    for i in range(N):
        dc_store = c - c[i,:]
        dc[:,i,:]= np.delete(dc_store,i,0)
    
    dr = np.sqrt(np.sum(dc**2,2))
    
    return dr,dc

def rules(N,dr,dc,v,s,r_r,dr_o,dr_a,alpha = 180):
    """This function implements the attraction rules for the different zones. The output is a matrix d which describes the change in direction in (x,y,z) coordinates for the each bird. The size if the d matrix is (N,3).
    ---
    Input parameters:
    N = number of birds
    dr = matrix which represents all distances from each bird to each other, size (N-1,N)
    dc = matrix which represents all distances from each bird to each other in (x,y,z) coordinates, size (N-1,N,3)
    v (optional) = the velocity in (x,y,z) coordinates of the birds
    r_r = size of the r-zone, where the birds repulse each other
    dr_o = size of the o-zone, where the birds align with each other
    dr_a = size of the a-zone, where the birds attract each other
    alpha (optional) = the angle of the cone which determines the visible part of sight of the bird
    """
    
    d = np.zeros([N,3])
    do = np.zeros([N,3])
    da = np.zeros([N,3])
    rr = mm.zone_check(N,dr,dc,r_r,dr_o,dr_a)
    ro = mm.zone_check(N,dr,dc,r_r,dr_o,dr_a,zone = 'o',alpha = alpha, v = v)
    ra = mm.zone_check(N,dr,dc,r_r,dr_o,dr_a,zone = 'a',alpha = alpha, v = v)

    #v_len = np.sqrt(v[:,0]**2+v[:,1]**2+v[:,2]**2)
    #v = v.T/v_len
    #v = v.T

    for i in range(N):
        #if there are birds in the r-zone:
        if int(np.sum(rr[:,i]))!=0:
            d_norm = dc[:,i,:].T/dr[:,i] 
            d_norm = d_norm.T
            d_store = -np.tensordot(d_norm,rr[:,i],(0,0))
            d[i,:] = d_store/np.sqrt(np.sum(d_store**2))
        #if there are birds in the o-zone:
        elif int(np.sum(ro[:,i]))!=0 and int(np.sum(ra[:,i]))==0: #(is just an else statement not better?)
            ro_store = np.insert(ro[:,i],i,0)
            d_store = np.tensordot(v/s,ro_store,(0,0))
            d[i,:] = d_store/np.sqrt(np.sum(d_store**2))
        #if there are birds in the a-zone:
        elif int(np.sum(ro[:,i]))==0 and int(np.sum(ra[:,i]))!=0:
            d_norm = dc[:,i,:].T/dr[:,i] 
            d_norm = d_norm.T
            d_store = np.tensordot(d_norm,ra[:,i],(0,0))
            d[i,:] = d_store/np.sqrt(np.sum(d_store**2))
        #if there are birds in both zones:
        elif int(np.sum(ro[:,i]))!=0 and int(np.sum(ra[:,i]))!=0: 
            ro_store = np.insert(ro[:,i],i,0)
            do[i,:] = np.tensordot(v/s,ro_store,(0,0))        
            d_norm = dc[:,i,:].T/dr[:,i] 
            d_norm = d_norm.T
            da[i,:] = np.tensordot(d_norm,ra[:,i],(0,0))
            d_store = (do[i,:]+da[i,:])/2
            d[i,:] = d_store/np.sqrt(np.sum(d_store**2))
        #if the birds are outside all zones
        else:
            d[i,:] = v[i,:]/s
            
    if np.size(d) < 3:
        print('rul: size of d is lower than 3')
        d = 0
    elif np.size(d)>3:
        if d.shape != (N,3):
            print('rul: direction vector has wrong shape', d.shape)
        if np.any(np.sqrt(np.sum(d**2,1))) > 1:
            print('rul: direction vector is not a unit vector')
    return d

def velocity(N,d,v,s,sigma,turning_rate,tau):
    """This function gives the new velocity v of the birds. 
    ---
    Input parameters:
    N = number of birds (size: N)
    d = desired direction of each bird (unit vector) (N,3)
    v = veloctiy of each bird (N,3)
    s = magnitude of velocity of each bird (1)
    sigma = error of the bird in estimating the desired direction (1)
    turning_rate: maximum rotation angle the bird can rotate per time (1)
    tau = time step of the simulation (1)
    """
    if np.size(d) < 3:
        print('vel: size of d is lower than 3')
        d = 0
    elif np.size(d)>3:
        if d.shape != (N,3):
            print('vel: direction vector has wrong shape', d.shape)
        if np.any(np.sqrt(np.sum(d**2,1))) > 1:
            print('vel: direction vector is not a unit vector')
    if np.size(v) < 3:
        print('vel: size of v is lower than 3')
    elif np.size(v) > 3:
        if v.shape != (N,3):
            print('vel: velocity vector has wrong shape', v.shape)
        if np.any(np.sqrt(np.sum(v**2,1))) > s:
            print('vel: velocity has wrong magnitude')
        
    #determine the angle between desired direction d and actual direction v:
    theta = np.arccos(np.clip(np.einsum('...j,...j',d,v/s),-1,1))
    #add stochaistic error:
    theta_rand = np.random.normal(theta,scale = sigma)
    #estimate maximum angle the bird can rotate in one time step and set this treshold for theta_rand:
    max_angle = turning_rate*tau
    theta_rand_max = np.clip(theta_rand,-max_angle,max_angle)
    
    #calculate d_new using a rotation matrix:
    d_new = rt.rotation(d,v/s,theta_rand_max)
    
    if d_new.shape != (N,3):
        print('vel: the new direction vector has wrong shape', d_new.shape)
    if np.any(np.sqrt(np.sum(d_new**2,1))) > 1:
        print('vel: new direction vector is no unit vector')
    return d_new
    
    