import initialize as init
import model as md
import numpy as np
import write_to_file as wf
import pickle

def execute(N,r_r,dr_o,dr_a,s,angle,sigma,turning_rate,tau,T,name,file = True):
    [c_0,v_0] = init.init(N,r_r,dr_o,dr_a,s)
    c = np.zeros([N,3,T])
    c[:,:,0] = c_0
    v = np.zeros([N,3,T])
    v[:,:,0] = v_0
    for i in range(T-1):    
        [dr, dc] = md.r_ij(N,r_r,dr_o,dr_a,c[:,:,i])
        d = md.rules(N,dr,dc,v[:,:,i],s,r_r,dr_o,dr_a,alpha = angle)
        d_new = md.velocity(N,d,v[:,:,i],s,sigma,turning_rate,tau)
        v[:,:,i+1] = d_new*s
        c[:,:,i+1] = c[:,:,i] + v[:,:,i]*tau
    
    if file == False:
        return c,v
    elif file == True:
        outfile = open('%s_c'%(name), 'wb')
        pickle.dump(c,outfile)
        outfile.close()
        outfile = open('%s_v'%(name), 'wb')
        pickle.dump(v,outfile)
        outfile.close()
        wf.write_param(N,r_r,dr_o,dr_a,s,angle,sigma,turning_rate,tau,T,name)
    else:
        print('exe: wrong input for "file", should be "True" or "False"')