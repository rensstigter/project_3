import numpy as np

def write_param(N,r_r,dr_o,dr_a,s,angle,sigma,turning_rate,tau,T,name):
    """    
    """ 
    file = open('%s.txt'%(name),'w+')
    file.write('N:%f \n'%(N))
    file.write('r_r:%f \n'%(r_r))
    file.write('dr_o:%f \n'%(dr_o))
    file.write('dr_a:%f \n'%(dr_a))
    file.write('s:%f \n'%(s))
    file.write('angle:%f \n'%(angle))
    file.write('sigma:%f \n'%(sigma))
    file.write('turning_rate:%f \n'%(turning_rate))
    file.write('tau:%f \n'%(tau))
    file.write('T:%f \n'%(T))
    file.flush()
    file.close