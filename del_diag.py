import numpy as np

def del_diag(A):
    """This function removes the diagnoal of a matrix A and moves all other element vertically to obtain the new matrix."""
    A=np.transpose(np.transpose(A)[~np.eye(A.shape[0],dtype=bool)].reshape((A.shape[0]),A.shape[1]-1))
    return A
