import numpy as np
import six

def zone_check(N,dr,dc,r_r,dr_o,dr_a, zone = "r", alpha = 180, v = 0):
    """This function checks for each bird if there are neighbouring birds in a certain zone (denoted by r,o or a). The output is a (N-1,N) boolean matrix with 1 if the bird is within that zone and 0 if it isn't. To implement a non-visible cone with angle alpha in the direction of the tail of the bird, one needs the velocity matrix v.
    ---
    Input parameters:
    N = number of birds 
    dr = matrix which represents all distances from each bird to each other, size (N-1,N)
    dc = matrix which represents all distances from each bird to each other in (x,y,z) coordinates, size (N-1,N,3)
    r_r = size of the r-zone, where the birds repulse each other
    dr_o = size of the o-zone, where the birds align with each other
    dr_a = size of the a-zone, where the birds attract each other
    zone (optional) = a region which has a certain meaning for the behaviour of one bird with respect to another bird, denoted by r,o or a
    alpha (optional) = the angle of the cone which determines the visible part of sight of the bird
    v (optional) = the velocity in (x,y,z) coordinates of the birds
    """
    
    birds_in_zone = 0
    
    if isinstance(zone,six.string_types):
        if zone == 'r':
            r1 = 0
            r2 = r_r
            #find the number of birds in the zone:
            birds_in_zone = (dr >= r1)*(dr < r2)
        elif zone == 'o' or 'a':
            #check if a value for v is given:
            if np.any(v != 0):
                if v.shape != (N,3):
                    print('mm: wrong shape of the velocity vector. It should be',(N,3), v.shape)
                    Bool = 1
                else:
                    #find the velocity direction vector of each bird (size = (N,3)):
                    v_length = np.sqrt(np.sum(v**2,1))
                    v_dir = (v.T/v_length).T

                    #find the direction vector from each bird to all other birds (size = (N-1,N,3)):
                    dr_inv = 1/dr
                    dc_dir = (dc.reshape((N-1)*N,3)*dr_inv.reshape((N-1)*N,1)).reshape(N-1,N,3)

                    #determine the angle theta between the direction of one bird and the direction of all birds with respect to that one bird 
                    #(size = (N-1,N)):
                    theta = np.arccos(np.clip(np.einsum('...k,...k',dc_dir,v_dir),-1,1))
                    theta_deg = theta*180/np.pi

                    #keep only the birds within the scope defined by the angle alpha:
                    Bool = theta_deg < alpha
            else:
                Bool = 1
            if zone == 'o':
                r1 = r_r
                r2 = r_r + dr_o
                birds_in_zone = (dr >= r1)*(dr < r2)*Bool
            else:
                r1 = r_r + dr_o
                r2 = r_r + dr_o + dr_a
                birds_in_zone = (dr >= r1)*(dr <= r2)*Bool
        else:
            print('This zone is not defined, choose r,o or a')
    else:
        print('The zone should be a string with values r,o or a')
    
    if birds_in_zone.shape != (N-1,N):
        print('mm: something went wrong, wrong shape of birds_in_zone', birds_in_zone.shape)
    
    return birds_in_zone
    

   
    
    
    